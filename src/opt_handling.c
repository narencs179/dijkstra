#define _GNU_SOURCE

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include "../include/npcl_system.h"

#include "opt_handling.h"

opt_struct opt_struct_create(char *base_net_file, char *node_weight_file, char *edge_weight_file, char *output_path,
        double percentile, long int pq_size,
        //double val_bound,
        double lower_bound, double upper_bound, long int pthreads, long int min_edges, int decimals,
        char *nwtransform, char *ewmetric, char *ewtransform) {
    opt_struct tmp;
    tmp.base_net_file = NULL;
    tmp.node_weight_file = NULL;
    tmp.edge_weight_file = NULL;
    tmp.output_path = NULL;
    tmp.ewmetric = NULL;
    tmp.ewtransform = NULL;
    tmp.nwtransform = NULL;

    if (base_net_file != NULL) {
        if (!npcl_file_exists(base_net_file)) {
            fprintf(stderr, "The input base network file |%s| does not exist. Exiting program...\n", base_net_file);
            exit(1);
        }
        asprintf(&tmp.base_net_file     , "%s", base_net_file);
    }

    if (node_weight_file != NULL) {
        if (!npcl_file_exists(node_weight_file)) {
            fprintf(stderr, "The input weighted node file |%s| does not exist. Exiting program...\n", node_weight_file);
            exit(1);
        }
        asprintf(&tmp.node_weight_file, "%s", node_weight_file);
    }

    if (edge_weight_file != NULL) {
        if (!npcl_file_exists(edge_weight_file)) {
            fprintf(stderr, "The input weighted edge file |%s| does not exist. Exiting program...\n", edge_weight_file);
            exit(1);
        }
        asprintf(&tmp.edge_weight_file, "%s", edge_weight_file);
    }

    if (output_path != NULL) {
        asprintf(&tmp.output_path, "%s", output_path);
    }

    if (ewmetric != NULL) {
        asprintf(&tmp.ewmetric, "%s", ewmetric);
    }

    if (ewtransform != NULL) {
        asprintf(&tmp.ewtransform, "%s", ewtransform);
    }

    if (nwtransform != NULL) {
        asprintf(&tmp.nwtransform, "%s", nwtransform);
    }

    tmp.percentile = percentile;
    tmp.pq_size  = pq_size;
    tmp.lower_bound = lower_bound;
    tmp.upper_bound = upper_bound;
//    tmp.val_bound = val_bound;
    tmp.pthreads  = pthreads;
    tmp.min_edges = min_edges;
    tmp.decimals = decimals;

    return  tmp;
}

void opt_struct_free(opt_struct *ostruct) {
    free(ostruct->base_net_file);
    free(ostruct->node_weight_file);
    free(ostruct->edge_weight_file);
    free(ostruct->output_path);
    free(ostruct->ewmetric);
    free(ostruct->ewtransform);
    free(ostruct->nwtransform);
}

void opt_struct_print(opt_struct *ostruct) {
    printf("BASE NET FILE         : %s\n" , ostruct->base_net_file     );
    printf("WEIGHTED EDGE FILE    : %s\n" , ostruct->edge_weight_file  );
    printf("WEIGHTED NODE FILE    : %s\n" , ostruct->node_weight_file  );
    printf("OUTPUT PATH           : %s\n" , ostruct->output_path       );
    printf("PERCENTILE            : %lf\n", ostruct->percentile        );
    printf("QUEUE SIZE            : %ld\n", ostruct->pq_size           );
//    printf("VAL BOUND             : %lf\n", ostruct->val_bound         );
    printf("VAL UPPER BOUND       : %lf\n", ostruct->upper_bound       );
    printf("VAL UPPER BOUND       : %lf\n", ostruct->lower_bound       );
    printf("PTHREADS              : %ld\n", ostruct->pthreads          );
    printf("MIN EDGES             : %ld\n", ostruct->min_edges         );
    printf("NDECIMALS             : %d\n" , ostruct->decimals          );
    printf("NODE WEIGHT TRANSFORM : %s\n" , ostruct->nwtransform       );
    printf("EDGE WEIGHT METRIC    : %s\n" , ostruct->ewmetric          );
    printf("EDGE WEIGHT TRANSFORM : %s\n" , ostruct->ewtransform       );
}

opt_struct options_parse(int argc, char **argv) {

    char EWMETRIC_DEFAULT[]    = "geom";
    char OUTPUT_PATH_DEFAULT[] = "results";
    double PERCENTILE_DEFAULT  = 0.05;
    long int PQ_SIZE_DEFAULT   = 100000;
//    double VAL_BOUND_DEFAULT   = 0;
    double LOWER_BOUND_DEFAULT = INFINITY;
    double UPPER_BOUND_DEFAULT = INFINITY;
    long int PTHREADS_DEFAULT  = 1;
    long int MIN_EDGES_DEFAULT = 2;
    int DECIMALS_DEFAULT       = 10;

    char *pthreads_err;
    char *min_edges_err;
    char *decimals_err;
    char *percentile_err;
    char *pq_size_err;
    char *upper_bound_err;
    char *lower_bound_err;
//    char *val_bound_err;

    char *base_net_file    = NULL;
    char *node_weight_file = NULL;
    char *edge_weight_file = NULL;
    char *output_path      = NULL;

    char *ewmetric    = NULL;
    char *nwtransform = NULL;
    char *ewtransform = NULL;

    double percentile  = PERCENTILE_DEFAULT;
    long int pq_size = PQ_SIZE_DEFAULT;
    double upper_bound = UPPER_BOUND_DEFAULT;
    double lower_bound = LOWER_BOUND_DEFAULT;
//    double val_bound = VAL_BOUND_DEFAULT;
    long int pthreads  = PTHREADS_DEFAULT;
    long int min_edges = MIN_EDGES_DEFAULT;
    int decimals  = DECIMALS_DEFAULT;

    char c;
    while ((c = getopt(argc, argv, "b:c:d:e:hl:m:n:o:p:q:v:w:")) != -1) {
        switch(c) {
            case 'b':
                base_net_file = optarg;
                break;
            case 'c':
                percentile = strtod(optarg, &percentile_err);
                if (percentile_err[0] != '\0') {
                    fprintf(stderr, "Please make sure that the input to -c is only numbers\n");
                    exit(1);
                }

                if ((percentile < 0) || (percentile > 100)) {
                    fprintf(stderr, "Accepted range for percentile (0, 100)\n");
                    exit(1);
                }
                break;
            case 'd':
                decimals = strtol(optarg, &decimals_err, 10);
                if (decimals_err[0] != '\0') {
                    fprintf(stderr, "Please make sure that the input to -d is only integers\n");
                    exit(1);
                }
                break;
            case 'e':
                ewtransform = strdup(optarg);
                break;
            case 'h':
                program_help();
                exit(1);
            case 'l':
                min_edges = strtol(optarg, &min_edges_err, 10);
                if (min_edges_err[0] != '\0') {
                    fprintf(stderr, "Please make sure that the input to -l is only integers\n");
                    exit(1);
                }
                break;
            case 'm':
                ewmetric = strdup(optarg);
                break;
            case 'n':
                nwtransform = strdup(optarg);
                break;
            case 'o':
                output_path = strdup(optarg);
                break;
            case 'p':
                pthreads = strtol(optarg, &pthreads_err, 10);
                if (pthreads_err[0] != '\0') {
                    fprintf(stderr, "Please make sure that the input to -p is only integers\n");
                    exit(1);
                }
                break;
            case 'q':
                pq_size = strtol(optarg, &pq_size_err, 10);
                if (pq_size_err[0] != '\0') {
                    fprintf(stderr, "Please make sure that the input to -q is only integers\n");
                    exit(1);
                }
                break;
            case 'v':
                 lower_bound = strtod(optarg, &lower_bound_err);
                if (lower_bound_err[0] != '\0') {
                    fprintf(stderr, "Please make sure that the input to -v is only numbers\n");
                    exit(1);
                }
                break;
           case 'w':
                upper_bound = strtod(optarg, &upper_bound_err);
                if (upper_bound_err[0] != '\0') {
                    fprintf(stderr, "Please make sure that the input to -w is only numbers\n");
                    exit(1);
                }
                break;
            case '?':
                fprintf(stderr, "ERROR!\n");
                fprintf(stderr, "Use the -h to get program help\n");
                exit(1);
        }
    }


    if (optind == argc) {
        fprintf(stderr, "Please provide either a edge weight file or a node weights file\n");
        exit(1);
    }

    if (base_net_file != NULL) {
        node_weight_file = argv[optind];
    } else {
        edge_weight_file = argv[optind];
    }

    if (ewmetric == NULL) {
        asprintf(&ewmetric, "%s", EWMETRIC_DEFAULT);
    }

    if (output_path == NULL) {
        asprintf(&output_path, "%s", OUTPUT_PATH_DEFAULT);
    }

    opt_struct opt = opt_struct_create(base_net_file, node_weight_file, edge_weight_file, output_path,
            percentile, pq_size, lower_bound, upper_bound, //val_bound,
            pthreads, min_edges, decimals, nwtransform, ewmetric, ewtransform);

    free(ewtransform);
    free(nwtransform);
    free(ewmetric);
    free(output_path);
    return(opt);
}

void program_help(void) {
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "   ---------------------------------------------------------------------------  \n");
    fprintf(stderr, "                Subgraph selection using Dijkstra Shortest Paths                \n");
    fprintf(stderr, "   ---------------------------------------------------------------------------  \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "                 Copyright (C) 2021 - Naren Chandran Sakthivel                  \n");
    fprintf(stderr, "                            <narenschandran@gmail.com>                          \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "      This program is free software: you can redistribute it and/or modify      \n");
    fprintf(stderr, "      it under the terms of the GNU General Public License as published by      \n");
    fprintf(stderr, "       the Free Software Foundation, either version 3 of the License, or        \n");
    fprintf(stderr, "                      (at your option) any later version.                       \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "        This program is distributed in the hope that it will be useful,         \n");
    fprintf(stderr, "         but WITHOUT ANY WARRANTY; without even the implied warranty of         \n");
    fprintf(stderr, "         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          \n");
    fprintf(stderr, "                  GNU General Public License for more details.                  \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "       You should have received a copy of the GNU General Public License        \n");
    fprintf(stderr, "     along with this program.  If not, see <http://www.gnu.org/licenses/>.      \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "  ----------------------------------------------------------------------------  \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    Usage:                                                                      \n");
    fprintf(stderr, "        edgser [options] <edge weight file>                                     \n");
    fprintf(stderr, "  (OR)  edgser [options] -b <base network> <node weight file>                   \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    Options:                                                                    \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -b    Base network file                                                     \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -c    Percentile cutoff                                                     \n");
    fprintf(stderr, "          Used to determine normalized path cost value to select top paths.     \n");
    fprintf(stderr, "          [Default: 0.05]                                                       \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -d    Number of decimal places                                              \n");
    fprintf(stderr, "          Node weights, edge weights and normalized path costs are rounded to   \n");
    fprintf(stderr, "          the given number of decimal places. Any calculation that is done      \n");
    fprintf(stderr, "          with these entities will only be done after they're rounded to the    \n");
    fprintf(stderr, "          correct number of decimal places.                                     \n");
    fprintf(stderr, "          [Default: 10]                                                         \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -e    Edge weight transformation                                            \n");
    fprintf(stderr, "          See transformation metrics section below.                             \n");
    fprintf(stderr, "          [Default: None]                                                       \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -h    Print this help                                                       \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -l    Minimum path length (in number of edges)                              \n");
    fprintf(stderr, "          Minimum number of edges required to select path.                      \n");
    fprintf(stderr, "          [Default: 2]                                                          \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -m    Edge weight calculation metric                                        \n");
    fprintf(stderr, "          If node weight file is given, this will be used to determine how      \n");
    fprintf(stderr, "          the edge weights are calculated. The details are as follows:          \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "          max    : Maximum node weight                                          \n");
    fprintf(stderr, "          mu     : Arithmentic mean of node weights                             \n");
    fprintf(stderr, "          geom   : Geometric mean of node weights                               \n");
    fprintf(stderr, "          harm   : Harmonic mean of node weights                                \n");
    fprintf(stderr, "          min    : Minimum node weight                                          \n");
    fprintf(stderr, "          simple : Special option used to implement path costs as simple        \n");
    fprintf(stderr, "                   summation of node weights.                                   \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "          Each option also has an associated inverse option that is invoked by  \n");
    fprintf(stderr, "          the prefix 'inv_' (Ex: inv_max, inv_mu, inv_geom, etc.) where the     \n");
    fprintf(stderr, "          weights are calculated by dividing 1 by the value calculated by       \n");
    fprintf(stderr, "          max, mu, geom, etc. For the simple case, this means that the path     \n");
    fprintf(stderr, "          cost is the sum of inverse of node weights.                           \n");
    fprintf(stderr, "          [Default: geom]                                                       \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -n    Node weight transformation                                            \n");
    fprintf(stderr, "          See transformation metrics section below.                             \n");
    fprintf(stderr, "          [Default: None]                                                       \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -o    Output path                                                           \n");
    fprintf(stderr, "          [Default: <working directory>/results]                                \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -p    Number of threads                                                     \n");
    fprintf(stderr, "          [Default: 1]                                                          \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -q    Queue size                                                            \n");
    fprintf(stderr, "          Number of top paths to keep in memory while running the program.      \n");
    fprintf(stderr, "          Set a value higher than the number of paths expected with the input   \n");
    fprintf(stderr, "          percentile cutoff.                                                    \n");
    fprintf(stderr, "          [Default: 100000]                                                     \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -v    Node weight lower bound                                               \n");
    fprintf(stderr, "          Value that lower bounds the node weights.                             \n");
    fprintf(stderr, "          [Default: Not used]                                                   \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    -w    Node weight upper bound                                               \n");
    fprintf(stderr, "          Value that lower bounds the node weights.                             \n");
    fprintf(stderr, "          [Default: Not used]                                                   \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "    Transformation metrics:                                                     \n");
    fprintf(stderr, "    -----------------------                                                     \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "        shift1:         value  - minval  +  1                                   \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "        shift1r:        maxval - value   +  1                                   \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "        mmax_scale:     value  - minval                                         \n");
    fprintf(stderr, "                        ---------------  +  1                                   \n");
    fprintf(stderr, "                        maxval - minval                                         \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "        mmaxr_scale:    maxval - value                                          \n");
    fprintf(stderr, "                        --------------   +  1                                   \n");
    fprintf(stderr, "                        maxval - minval                                         \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "                                                                                \n");
    fprintf(stderr, "        raise2:        2 ^ value                                                \n");
    fprintf(stderr, "                                                                                \n");
}
