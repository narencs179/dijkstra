typedef struct node_entry {
    char       *key    ;
    double      value  ;
    size_t      n      ;
} node_entry;

typedef struct edge_entry {
    char      *key      ;
    char      *nodeA    ;
    char      *nodeB    ;
    size_t     nodeA_id ;
    size_t     nodeB_id ;
    double     value    ;
    size_t     n        ;
} edge_entry;

node_entry create_node_entry(char *key, double value, size_t n);
void print_node_entry(node_entry node);

edge_entry create_edge_entry(char *nodeA, char *nodeB, size_t nodeA_id, size_t nodeB_id, double value, size_t n);
void free_edge_entry(edge_entry edge);
void print_edge_entry(edge_entry edge);
