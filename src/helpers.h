/*
 *
 * If you want to include this header file, make sure to also include
 * "npcl_pq.h", "igraph/igraph.h" and "helper_structs.h" before it
 *
 */


void read_nodefile(char *fname, node_entry **hash, char *sep);
void free_node_table(node_entry *node_table);
void free_node_entry(node_entry node);

void read_edgefile(char *weighted_edge_file, edge_entry **edge_table, node_entry **node_table, char *sep);

void setup_basenet(char *basenet_fpath, edge_entry **edge_table, char *node_fpath, node_entry **node_table, char *sep);
void free_edge_table(edge_entry *edge_table);

void setup_graph(edge_entry **edge_table, igraph_t *G, igraph_vector_t *wt_list);
void init_edge_res(igraph_vector_ptr_t *edge_res, size_t nnodes);
void free_edge_res(igraph_vector_ptr_t *edge_res);
void write_edge_res(igraph_vector_ptr_t *edge_res, FILE *fcon, edge_entry **edge_table, int min_edges, int ndec);

void edge_res_to_path_pq(igraph_vector_ptr_t *edge_res, npcl_path_pq *h, edge_entry **edge_table, int min_edges, long int maxpqsize, size_t *npaths, double add_wt, int ndec, double *hmax);
void pq_to_file(npcl_path_pq *h, char *spath_out_file, char *edge_out_file, char *node_out_file, edge_entry **edge_table, long int pq_size, long int rpaths, int ndec, size_t *totselect, double *finval, double *firstlose, int *saturated);
int compare(const void *a, const void *b);
double bankers_round(double val, long int decimals);
