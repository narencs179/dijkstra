# Need to check in a clean environment whether this is enough to get
# wgetwrapper and exwrapper working without any additional mucking about
# required.
# export PATH="$(pwd)/prereq":"$PATH"

# BISON FLEX LIBXML2 dependencies
bash prereq/libigraph_setup -b build -v 0.9.1
bash prereq/libigraph_static_setup -b build -v 0.9.1

libnpcl_dir=$(bash prereq/exwrapper -o build prereq/libnpcl-0.1.0.tar.gz)
( cd "$libnpcl_dir"; mkdir -p lib; mkdir -p include; make libnpcl )

mkdir -p lib
cp "${libnpcl_dir}/lib/"libnpcl* ./lib
cp 'build/lib/libigraph.so' 'build/lib/libigraph.so.0' 'build/lib/libigraph.a' ./lib

mkdir -p include
cp "${libnpcl_dir}/include/"npcl*.h ./include
cp -r 'build/include/igraph/' ./include/
cp prereq/stb_ds.h ./include/

mkdir -p bin
