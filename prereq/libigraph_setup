#! /usr/bin/env bash
# libigraph standalone installation script
# Author: Naren Chandran Sakthivel <narenschandran@gmail.com>

# This script was at least partially generated using m4 macros. This was done
# to make standalone install scripts for different software while at the same
# time writing each repeated portion of the code only once. If you're thinking
# of making changes to it, consider attempting to do it at the script
# generation step.

# This script was generated on 2021-04-19T22:04:09+00:00

set -u

NAME='libigraph'
version_table='version  url md5sum
0.9.1	https://github.com/igraph/igraph/releases/download/0.9.1/igraph-0.9.1.tar.gz	a1db1cd52d0ee6395be3451a8dea7c30'

print_available_versions() {
    echo "The available ${NAME} versions are: "
    echo "${version_table}" | awk 'NR > 1 {print $1}' | sort | uniq | sed ':a; N; $!ba; s/\n/, /g' >&2
}

quick_help() {
    echo "                                                                                    " >&2
    echo "    A (mostly) standalone script for obtaining ${NAME}                              " >&2
    echo "                                                                                    " >&2
    echo "                                                                                    " >&2
    echo "    Options:                                                                        " >&2
    echo "                                                                                    " >&2
    echo "                                                                                    " >&2
    echo "      -a: Base directory into which files will be downloaded into.                  " >&2
    echo "          [Default: ${archive_dir}]                                                 " >&2
    echo "                                                                                    " >&2
    echo "      -b: Directory into which the files/folder will be extracted into.             " >&2
    echo "          Consider this to be the bin directory for executables and                 " >&2
    echo "          lib directory for libraries.                                              " >&2
    echo "          Also check the -s option for adding symlinks of executables               " >&2
    echo "          into this directory (in case of software)                                 " >&2
    echo "          [Default: ${bin_dir}]                                                     " >&2
    echo "                                                                                    " >&2
    echo "      -d: Command to invoke wgetwrapper [Default: wgetwrapper]                      " >&2
    echo "                                                                                    " >&2
    echo "      -e: Command to invoke exwrapper   [Default: exwrapper]                        " >&2
    echo "                                                                                    " >&2
    echo "      -h: Prints this help message                                                  " >&2
    echo "                                                                                    " >&2
    echo "      -l: Lists all versions of $NAME that this script can install                  " >&2
    echo "                                                                                    " >&2
    echo "      -s: Available only for some software.If available, symlinks the               " >&2
    echo "          executables to the bin directory.                                         " >&2
    echo "                                                                                    " >&2
    echo "      -v: Specify version of $NAME to be installed.                                 " >&2
    echo "          [Default: $version]                                                       " >&2
    echo "                                                                                    " >&2
    echo "                                                                                    " >&2
    print_available_versions
    if ! [ -z "${specific_notes:-}" ]; then
        echo "                                                                                    " >&2
        echo "$specific_notes                                                                     " >&2
    fi
    echo "                                                                                    " >&2
}

cd_err() { echo "cd command has failed. Exiting..." >&2; exit 1; }

# By default, the first version number in the version_table will be used if
# no entry is given by the user.
version=$(echo "${version_table}" | awk 'NR == 2 {print $1}')
bin_dir=$(readlink -m 'bin')
archive_dir=$(readlink -m "archive")
SYMLINK='NO'
download_cmd='wgetwrapper'
extract_cmd='exwrapper'
while getopts "b:d:e:hlsv:" opt; do
    case $opt in
        a ) archive_dir=$(readlink -m "$OPTARG") ;;
        b ) bin_dir=$(readlink -m "$OPTARG") ;;
        d ) download_cmd="$OPTARG" ;;
        e ) extract_cmd="$OPTARG" ;;
        h ) quick_help;
            exit 1
            ;;
        l ) print_available_versions;
            exit
            ;;
        s ) SYMLINK='YES' ;;
        v ) version="$OPTARG" ;;
       \? ) quick_help;
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

version_check=$(echo "${version_table}" | awk -v ver="${version}" 'NR > 1 && $1 == ver {print $1}' | grep -c "$version")

if [ "$version_check" -eq "0" ]; then
    echo "${NAME} version ${version} is not available in the version table" >&2
    print_available_versions
    exit 1
elif [ "$version_check" -gt "1" ]; then
    echo "Multiple entries for the ${NAME} version ${version} is present in the" >&2
    echo "version table. Please recheck the version table file used to generate this" >&2
    echo "script." >&2
    exit 1
fi

echo "Obtaining ${NAME} ${version}" >&2

download_url=$(echo "${version_table}" | awk -v ver="${version}" 'NR > 1 && $1 == ver {print $2}')
md5_val=$(echo "${version_table}" | awk -v ver="${version}" 'NR > 1 && $1 == ver {print $3}')

download_dir="${archive_dir}/${NAME}/${version}"
mkdir -p "${download_dir}"

echo "Downloading file from URL: ${download_url}" >&2
echo "File is being downloaded to: ${download_dir}" >& 2
downloaded_file=$(${download_cmd} -o "${download_dir}" "${download_url}")
if [ -z "$downloaded_file" ]; then
    exit 1
fi
echo "File download successful: $downloaded_file" >&2

downloaded_md5=$(md5sum "$downloaded_file" | cut -f1 -d' ')
echo "Expected md5sum: $md5_val" >&2
echo "Actual md5sum: $downloaded_md5" >&2
if [ "$md5_val" == "$downloaded_md5" ]; then
    echo "md5 sums matched!" >&2
elif [ "$md5_val" != "$downloaded_md5" ]; then
    echo "md5 sums did not match! Exiting program..." >&2
    exit 1
fi

extracted_path=$(${extract_cmd} -o ${bin_dir} ${downloaded_file})
if [ -z "$extracted_path" ]; then
    exit 1
fi
echo "Extracted to directory: ${extracted_path}" >&2


# Mute the warning that are printed to state that some vertices are unreachable
# This saves me ~10 seconds in cases with 20k nodes
if [ "$version" = "0.9.1" ]; then
    sed -i '440s/if (to_reach > 0) {/\/\/if (to_reach > 0) {/g' "${extracted_path}/src/paths/dijkstra.c"
    sed -i '441s/IGRAPH_WARNING/\/\/IGRAPH_WARNING/g' "${extracted_path}/src/paths/dijkstra.c"
    sed -i '442s/}/\/\/}/g' "${extracted_path}/src/paths/dijkstra.c"
fi

cd "$extracted_path"
mkdir -p "build"
( cd build;
cmake .. \
    -DIGRAPH_USE_INTERNAL_BLAS=ON \
    -DIGRAPH_USE_INTERNAL_LAPACK=ON \
    -DIGRAPH_USE_INTERNAL_ARPACK=ON \
    -DIGRAPH_USE_INTERNAL_GLPK=ON  \
    -DIGRAPH_USE_INTERNAL_CXSPARSE=ON  \
    -DIGRAPH_USE_INTERNAL_GMP=ON  \
    -DIGRAPH_ENABLE_TLS=ON \
    -DIGRAPH_GRAPHML_SUPPORT=ON \
    -DCMAKE_INSTALL_PREFIX="${bin_dir}" \
    -DBUILD_SHARED_LIBS=ON;
cmake --build . ;
cmake --install . )
echo "$NAME $version is now available." >&2

exit 0
