#include <math.h>
#include <stdio.h>

#include "../include/stb_ds.h"

#include "helper_structs.h"
#include "wtfns.h"

double mu(double a, double b) {
    double val = (a + b) / 2;
    return val;
}

double invmu(double a, double b) {
    double val = 2 / (a + b);
    return val;
}

double geom_mu(double a, double b) {
    double val = sqrt(a * b);
    return val;
}

double invgeom_mu(double a, double b) {
    double val = 1 / sqrt(a * b);
    return val;
}

// I'm using this over the equivalent 2 / ( (1/a) + (1/b) ) form in the hope
// that floating point arithmetic is kinder on this formulation. I don't know
// if it is, just a hunch
double harm_mu(double a, double b) {
    double val = (2 * a * b) / (a + b);
    return val;
        
}

double invharm_mu(double a, double b) {
    double val = (a + b) / (2 * a * b);
    return val;
        
}

double min(double a, double b) {
    double val = ( (a) < (b) ) ? (a) : (b);
    return val;
}

double invmin(double a, double b) {
    double val = ( (a) < (b) ) ? (a) : (b);
    val = 1 / val;
    return val;
}
double max(double a, double b) {
    double val = ( (a) > (b) ) ? (a) : (b);
    return val;
}

double invmax(double a, double b) {
    double val = ( (a) > (b) ) ? (a) : (b);
    val = 1 / val;
    return val;
}

double simple_wt(double a, double b) {
    double val = (b);
    return val;
}

double invsimple_wt(double a, double b) {
    double val = 1 / b;
    return val;
}


void calculate_weights(node_entry **node_table, edge_entry **edge_table, char *metric) {
    double (*fn)(double, double);

    if (strcmp(metric, "max") == 0) {
        fn = &max;
    } else if (strcmp(metric, "invmax") == 0) {
        fn = &invmax;
    } else if (strcmp(metric, "mu") == 0) {
        fn = &mu;
    } else if (strcmp(metric, "invmu") == 0) {
        fn = &invmu;
    } else if (strcmp(metric, "geom") == 0) {
        fn = &geom_mu;
    } else if (strcmp(metric, "invgeom") == 0) {
        fn = &invgeom_mu;
    } else if (strcmp(metric, "harm") == 0) {
        fn = &harm_mu;
    } else if (strcmp(metric, "invharm") == 0) {
        fn = &invharm_mu;
    } else if (strcmp(metric, "min") == 0) {
        fn = &min;
    } else if (strcmp(metric, "invmin") == 0) {
        fn = &invmin;
    } else if (strcmp(metric, "simple") == 0) {
        fn = &simple_wt;
    } else if (strcmp(metric, "invsimple") == 0) {
        fn = &invsimple_wt;
    } else {
        fprintf(stderr, "Unknown metric: %s\n", metric);
        fprintf(stderr, "Must be one of min, invmin, harm, invharm, geom, invgeom, mu, invmu, max, invmax.\n");
        exit(1);
    }

    for (size_t i = 0; i < shlen((*edge_table)); i++) {
        edge_entry edge = (*edge_table)[i];
        size_t i1 = shgeti((*node_table), edge.nodeA);
        size_t i2 = shgeti((*node_table), edge.nodeB);

        if ( (i1 == -1) || (i2 == -1) ) {
            fprintf(stderr, "Node table is missing some nodes\n");
            exit(1);
        }

        size_t j1 = edge.nodeA_id;
        size_t j2 = edge.nodeB_id;

        if ( (i1 != j1) || (i2 != j2) ) {
            fprintf(stderr, "Node IDs are not harmonized. Wrong table?");
        }

        (*edge_table)[i].value = fn((*node_table)[i1].value, (*node_table)[i2].value);

    }

}
