typedef struct npcl_qstruct {
    double value;
    char   *key;
} npcl_qstruct;

typedef struct npcl_path {
    double *key;
    long int nedges;
    double pcost;
    double value; // Normalized path cost
} npcl_path;

typedef struct npcl_pq {
    long int nfilled;
    long int capacity;
    npcl_qstruct **list;
} npcl_pq;

typedef struct npcl_path_pq {
    long int nfilled;
    long int capacity;
    npcl_path **list;
} npcl_path_pq;


npcl_qstruct npcl_create_qstruct(char *key, double value);


void npcl_pq_init(npcl_pq *h, long int capacity);
void npcl_pq_free(npcl_pq *h);
void npcl_pq_fill(npcl_pq *h, npcl_qstruct *in_qs);
void npcl_pq_swap_entries(npcl_pq *h, long int i, long int j);
void npcl_pq_pop(npcl_pq *h);
void npcl_pq_keep_n(npcl_pq *h, long int n);

void npcl_pq_bubble_sort(npcl_pq *h, long int n);
void npcl_pq_bubble_sort_and_free(npcl_pq *h, long int n);

long int npcl_pq_lomuto_partition(npcl_pq *h, long int l, long int r, long int pi);
double npcl_pq_select(npcl_pq *h, long int l, long int r, long int k);


npcl_path npcl_create_path(double *key, long int nedges, double pcost, double value);

void npcl_path_pq_init(npcl_path_pq *h, long int capacity);
void npcl_path_pq_fill(npcl_path_pq *h, npcl_path *in_path);
void npcl_path_pq_free(npcl_path_pq *h);
void npcl_path_pq_swap_entries(npcl_path_pq *h, long int i, long int j);
void npcl_path_pq_pop(npcl_path_pq *h);
void npcl_path_pq_keep_n(npcl_path_pq *h, long int n);
long int npcl_path_pq_lomuto_partition(npcl_path_pq *h, long int l, long int r, long int pi);
double npcl_path_pq_select(npcl_path_pq *h, long int l, long int r, long int k);

