#define get_max(htable, maxval) { \
    if (shlen((htable)) < 1) { \
        fprintf(stderr, "Input hash does not have a single element...\n"); \
        exit(1); \
    } \
    double max = (htable)[0].value; \
    for (size_t i = 1; i < shlen((htable)); i++) { \
        if (max < (htable)[i].value) { \
            max = (htable)[i].value; \
        } \
    } \
    maxval = max; \
}

#define get_min(htable, minval) { \
    if (shlen((htable)) < 1) { \
        fprintf(stderr, "Input hash does not have a single element...\n"); \
        exit(1); \
    } \
    double min = (htable)[0].value; \
    for (size_t i = 1; i < shlen((htable)); i++) { \
        if (min > (htable)[i].value) { \
            min = (htable)[i].value; \
        } \
    } \
    minval = min; \
}


#define raise2(htable) { \
    for (size_t i = 0; i < shlen((htable)); i++) { \
        (htable)[i].value = pow(2, (htable)[i].value); \
    } \
}

#define shift1(htable, maxval, minval) { \
    for (size_t i = 0; i < shlen((htable)); i++) { \
        (htable)[i].value = (((htable)[i].value - (minval))) + 1; \
    } \
}

#define shift1r(htable, maxval, minval) { \
    for (size_t i = 0; i < shlen((htable)); i++) { \
        (htable)[i].value = (((maxval) - (htable)[i].value)) + 1; \
    } \
}



#define mmax(htable, maxval, minval) { \
    for (size_t i = 0; i < shlen((htable)); i++) { \
        (htable)[i].value = (((htable)[i].value - (minval)) / ((maxval) - (minval))) + 1; \
    } \
}

#define mmaxr(htable, maxval, minval) { \
    for (size_t i = 0; i < shlen((htable)); i++) { \
        (htable)[i].value = (((maxval) - (htable)[i].value) / ((maxval) - (minval))) + 1; \
    } \
}

// This definition used to do have the rounding functionality done here.
// It's changed now to use a function. In order to not disturb the macro
// that rounds values in hash tables, this defintion remains even though
// it can be replaced
#define round_val(val, ndecimals) { \
    val = bankers_round(val, ndecimals); \
} \

#define round_vals(htable, ndecimals) { \
    for (size_t i = 0; i < shlen((htable)); i++) { \
        round_val(((htable)[i].value), ndecimals); \
    } \
}

#define bound_val(val, lower_bound, upper_bound) { \
    if ( (val) > (upper_bound) ) { \
        val = upper_bound; \
    } else if ( (val) < (lower_bound) ) { \
        val = lower_bound; \
    } \
}

// Extra braces for safety
#define bound_vals(htable, lower_bound, upper_bound) { \
    for (size_t i = 0; i < shlen((htable)); i++) { \
        bound_val((((htable)[i].value)), (lower_bound), (upper_bound)); \
    } \
}
