static:
#	gcc -Llib -static -Wall -g -fPIC -fopenmp -o bin/edgser_singlethread \
		src/dijkstra_singlethread.c \
		src/helper_structs.c \
		src/helpers.c src/wtfns.c \
		src/outfn.c \
		src/opt_handling.c \
		-lnpcl -ligraph -lm; 
	gcc -Llib -static -Wall -g -fPIC -fopenmp -o bin/edgser \
		src/dijkstra_multithread.c \
		src/helper_structs.c \
		src/helpers.c src/wtfns.c \
		src/outfn.c \
		src/opt_handling.c \
		-lnpcl -ligraph -lm;

shared:
#	gcc -Llib -Wall -g -fPIC -fopenmp -o bin/edgser_singlethread.shared \
		src/dijkstra_singlethread.c \
		src/helper_structs.c \
		src/helpers.c src/wtfns.c \
		src/outfn.c \
		src/opt_handling.c \
		-lnpcl -ligraph -lm; 
	gcc -Llib -Wall -g -fPIC -fopenmp -o bin/edgser.shared \
		src/dijkstra_multithread.c \
		src/helper_structs.c \
		src/helpers.c src/wtfns.c \
		src/outfn.c \
		src/opt_handling.c \
		-lnpcl -ligraph -lm;

