#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "../include/stb_ds.h"

#include "helper_structs.h"
#include "opt_handling.h"

void write_nodekey_table(node_entry **node_table, char *fpath) {

    FILE *fcon = fopen(fpath, "w");
    if (fcon == NULL) {
        fprintf(stderr, "Unable to write node table to %s. Exiting...\n", fpath);
        exit(1);
    }

    fprintf(fcon, "node\tid\n");
    for (size_t i = 0; i < shlen((*node_table)); i++) {
        node_entry N = (*node_table)[i];
        fprintf(fcon, "%s\t%ld\n", N.key, i);
    }
    fclose(fcon);
}

void write_node_weights(node_entry **node_table, char *fpath, int ndec) {
    FILE *fcon = fopen(fpath, "w");

    if (fcon == NULL) {
        fprintf(stderr, "Unable to write node table to %s. Exiting...\n",
                fpath);
        exit(1);
    }

    fprintf(fcon, "node\tweight\tid\n");
    for (size_t i = 0; i < shlen((*node_table)); i++) {
        node_entry N = (*node_table)[i];
        fprintf(fcon, "%s\t%.*lf\t%ld\n", N.key, ndec, N.value, i);
    }

    fclose(fcon);
}

void write_edge_weights(edge_entry **edge_table, char *fpath, int ndec) {
    FILE *fcon = fopen(fpath, "w");

    if (fcon == NULL) {
        fprintf(stderr, "Unable to write edge table to %s. Exiting...\n",
                fpath);
        exit(1);
    }

    fprintf(fcon, "nodeA\tnodeB\tweight\tid\n");
    for (size_t i = 0; i < shlen((*edge_table)); i++) {
        edge_entry E = (*edge_table)[i];
        fprintf(fcon, "%s\t%s\t%.*lf\t%ld\n", E.nodeA, E.nodeB, ndec, E.value, i);
    }

    fclose(fcon);
}
void write_spath_log(int argc, char **argv, opt_struct *opt, char *fpath, size_t totpaths, size_t totselect, double finval, double firstlose, double ttaken, int saturated, long int nnodes, long int nedges) {
    FILE *fcon = fopen(fpath, "w");

    if (fcon == NULL) {
        fprintf(stderr, "Unable to write to log file.\n");
        exit(1);
    }

    fprintf(fcon, "Command:");
    for (size_t i = 0; i < argc; i++) {
        fprintf(fcon, " %s", argv[i]);
    }

    fprintf(fcon, "\n\n");
    fprintf(fcon, "Input files:\n");
    fprintf(fcon, "------------\n");
    if (opt->base_net_file != NULL) {
        fprintf(fcon, "Base network: %s\n", opt->base_net_file);
    } else {
        fprintf(fcon, "Base network: none\n");
    }

    if (opt->node_weight_file != NULL) {
        fprintf(fcon, "Node weight file: %s\n", opt->node_weight_file);
    } else {
        fprintf(fcon, "Node weight file: none\n");
    }

    if (opt->edge_weight_file != NULL) {
        fprintf(fcon, "Edge weight file: %s\n", opt->edge_weight_file);
    } else {
        fprintf(fcon, "Edge weight file: none\n");
    }

    fprintf(fcon, "Number of nodes: %ld\n", nnodes);
    fprintf(fcon, "Number of edges: %ld\n", nedges);

    fprintf(fcon, "\n");
    fprintf(fcon, "Output path: %s\n", opt->output_path);
    fprintf(fcon, "------------\n");

    fprintf(fcon, "\n");
    fprintf(fcon, "Run parameters:\n");
    fprintf(fcon, "---------------\n");
    fprintf(fcon, "Mininum number of edges: %ld\n", opt->min_edges);
    fprintf(fcon, "Percentile cutoff: %lf\n", opt->percentile);
    fprintf(fcon, "Queue size: %ld\n", opt->pq_size);
    fprintf(fcon, "Maximum decimal places: %d\n", opt->decimals);
    fprintf(fcon, "Number of threads: %ld\n", opt->pthreads);

    if (isfinite(opt->lower_bound)) {
        fprintf(fcon, "Node weight lower bound: %lf\n", opt->lower_bound);
    } else {
        fprintf(fcon, "Node weight lower bound: none\n");
    }

    if (isfinite(opt->upper_bound)) {
        fprintf(fcon, "Node weight upper bound: %lf\n", opt->upper_bound);
    } else {
        fprintf(fcon, "Node weight upper bound: none\n");
    }

    if (opt->nwtransform != NULL) {
        fprintf(fcon, "Node weight transformation: %s\n", opt->nwtransform);
    } else {
        fprintf(fcon, "Node weight transformation: none\n");
    }

    if (opt->ewmetric != NULL) {
        fprintf(fcon, "Edge weight calculation: %s\n", opt->ewmetric);
    } else {
        fprintf(fcon, "Edge weight calculation: none\n");
    }

    if (opt->ewtransform != NULL) {
        fprintf(fcon, "Edge weight transformation: %s\n", opt->ewtransform);
    } else {
        fprintf(fcon, "Edge weight transformation: none\n");
    }

    fprintf(fcon, "\n");
    fprintf(fcon, "Output summary:\n");
    fprintf(fcon, "---------------\n");
    fprintf(fcon, "Time taken: %.3lf seconds\n", ttaken);
    fprintf(fcon, "Total number of shortest paths that meet path length criteria: %ld\n", totpaths);
    fprintf(fcon, "Normalized path cost cutoff: %.*lf\n", opt->decimals, finval);
    fprintf(fcon, "Paths that meet the cutoff requirements: %ld\n", totselect);
    fprintf(fcon, "First normalized path costs that misses the cutoff: %lf\n", firstlose);
    if (saturated) {
        fprintf(fcon, "\n");
        fprintf(fcon, "Saturation was reached when searching for paths that are below the given normalized path cutoff. Consider rerunning the program using a higher queue size or do not take paths with the maximum normalized path cost for safety.\n");
    }

    fprintf(fcon, "\n");
    fprintf(fcon, "Program has finished!\n");

    fclose(fcon);
}
