#define NPCL_PATH_MAX 1024

int npcl_file_exists(const char *fpath);
int npcl_file_readable(const char *fpath);
int npcl_file_writable(const char *fpath);
int npcl_dir_exists(char *path);
char *npcl_getwd (void);
char* npcl_absolute_path(char *path);
void npcl_dir_create(char *path);
void npcl_dir_create_recursive(char *path);
char *npcl_basename(char *path);
char* npcl_dirname(char* path);
char* npcl_basename_noext(char *path);
char* npcl_path_join(char* path_head, char* path_comp);
