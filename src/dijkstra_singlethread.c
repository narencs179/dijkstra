#define  STB_DS_IMPLEMENTATION
#define  _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h> // Just for wallclock time using omp_get_wtime()

#include "../include/stb_ds.h"
#include "../include/npcl_pq.h"
#include "../include/npcl_system.h"
#include "../include/igraph/igraph.h"

#include "helper_structs.h"
#include "helpers.h"
#include "wtfns.h"
#include "opt_handling.h"
#include "outfn.h"
#include "ezmacros.h"

int main(int argc, char **argv) {

    double begin = omp_get_wtime();
    opt_struct opt = options_parse(argc, argv);

    int is_simple     = strcmp(opt.ewmetric, "simple") == 0 ;
    int is_invsimple = strcmp(opt.ewmetric, "invsimple") == 0;
    if ((is_simple) || (is_invsimple)) {
        if (opt.node_weight_file == NULL) {                                      
            fprintf(stderr, "The simple and invsimple metrics can only be used if a node weight file is given.\n");
            exit(1);                                                             
        }                                                                        
                                                                                 
        if (opt.ewtransform != NULL) {                                           
            fprintf(stderr, "The simple and invsimple metrcis can only be used if no edge transformation is given.\n");
            exit(1);
        }                                                                        
    }

    char  sep[]        = "\t";

    node_entry *nodes  = NULL;
    edge_entry *edges  = NULL;
    char *output_path = npcl_absolute_path(opt.output_path);
    npcl_dir_create_recursive(output_path);

    char *bname;

    if (opt.edge_weight_file == NULL) {
        bname = npcl_basename_noext(opt.node_weight_file);
        setup_basenet(opt.base_net_file, &edges, opt.node_weight_file, &nodes, sep);

        double mn;
        get_min(nodes, mn);

        double mx;
        get_max(nodes, mx);


        if ( isfinite(opt.lower_bound) || isfinite(opt.upper_bound) )  {
            double bound_mn;
            if (isfinite(opt.lower_bound)) {
                bound_mn = opt.lower_bound;
            } else {
                bound_mn = mn;
            }

            double bound_mx;
            if (isfinite(opt.upper_bound)) {
                bound_mx = opt.upper_bound;
            } else {
                bound_mx = mx;
            }

            if (mn > mx) {
                fprintf(stderr, "The lower bound is greater than the upper bound. Please enter proper values.\n");
                exit(1);
            }

            bound_vals(nodes, bound_mn, bound_mx);
        }

        get_min(nodes, mn);
        get_max(nodes, mx);

        if (opt.nwtransform != NULL) {
            if (strcmp(opt.nwtransform, "raise2") == 0) {
                raise2(nodes);
            } else if (strcmp(opt.nwtransform, "shift1") == 0) {
                shift1(nodes, mx, mn);
            } else if (strcmp(opt.nwtransform, "shift1r") == 0) {
                shift1r(nodes, mx, mn);
            } else if (strcmp(opt.nwtransform, "mmax") == 0) {
                mmax(nodes, mx, mn);
            } else if (strcmp(opt.nwtransform, "mmaxr") == 0) {
                mmaxr(nodes, mx, mn);
            } else {
                fprintf(stderr, "Acceptable node weight transformations are: raise2, shift1, shift1r, mmax, mmaxr.\n");
                exit(1);
            }
        }

        calculate_weights(&nodes, &edges, opt.ewmetric);
        round_vals(nodes, opt.decimals);

        get_min(nodes, mn);
        get_max(nodes, mx);

        if ((mn < 0) && (opt.nwtransform == NULL)) {
            fprintf(stderr, "Negative node weights observed in the node weight table. ");
            fprintf(stderr, "Please select a node weight transformation that makes all the values non-negative.\n");
            exit(1);
        }


        char *nodeweight_outfname;
        asprintf(&nodeweight_outfname, "%s_%s", bname, "input_nodes.tsv");

        char *nodeweight_outfile = npcl_path_join(output_path, nodeweight_outfname);
        
        write_node_weights(&nodes, nodeweight_outfile, opt.decimals);

        free(nodeweight_outfname);
        free(nodeweight_outfile);

    } else {

        read_edgefile(opt.edge_weight_file, &edges, &nodes, sep);

        bname = npcl_basename_noext(opt.edge_weight_file);

        char *nodekey_outfname;
        asprintf(&nodekey_outfname, "%s_%s", bname, "input_nodes.tsv");

        char *nodekey_outfile = npcl_path_join(output_path, nodekey_outfname);
        write_nodekey_table(&nodes, nodekey_outfile);

        free(nodekey_outfname);
        free(nodekey_outfile);
    }

    round_vals(edges, opt.decimals);

    double emn;
    get_min(edges, emn);

    double emx;
    get_max(edges, emx);
        
    if ((emn < 0) && (opt.ewtransform == NULL)) {
        fprintf(stderr, "Negative node weights observed in the node weight table. ");
        fprintf(stderr, "Please select a node weight transformation that makes all the values non-negative.\n");
        exit(1);
    }

    if (opt.ewtransform != NULL) {
        if (strcmp(opt.ewtransform, "raise2") == 0) {
            raise2(edges);
        } else if (strcmp(opt.ewtransform, "shift1") == 0) {
            shift1(edges, emx, emn);
        } else if (strcmp(opt.ewtransform, "shift1r") == 0) {
            shift1r(edges, emx, emn);
        } else if (strcmp(opt.ewtransform, "mmax") == 0) {
            mmax(edges, emx, emn);
        } else if (strcmp(opt.ewtransform, "mmaxr") == 0) {
            mmaxr(edges, emx, emn);
        } else {
            fprintf(stderr, "Acceptable edge weight transformations are: raise2, shift1, shift1r, mmax, mmaxr.\n");
            exit(1);
        }
    }

    char *edge_weight_outfname;
    asprintf(&edge_weight_outfname, "%s_%s", bname, "input_edges.tsv");
    char *edge_weight_outfile = npcl_path_join(output_path, edge_weight_outfname, opt.decimals);

    write_edge_weights(&edges, edge_weight_outfile);
    free(edge_weight_outfname);
    free(edge_weight_outfile);

    igraph_vector_t ewt_list;
    igraph_t G;

    setup_graph(&edges, &G, &ewt_list);

    long int nedges = shlen(edges);
    edge_entry *edges2 = malloc(sizeof(edge_entry) * nedges);

    for (size_t i = 0; i < nedges; i++) {
        edge_entry e = edges[i];
        edges2[i] = create_edge_entry(e.nodeA, e.nodeB, e.nodeA_id, e.nodeB_id, e.value, e.n);
    }

    size_t nnodes = shlen(nodes);
    node_entry *nodes2 = malloc(sizeof(node_entry) * nnodes);

    for (size_t i = 0; i < nnodes; i++) {
        node_entry n = nodes[i];
        nodes2[i] = create_node_entry(n.key, n.value, n.n);
    }

    free_node_table(nodes);
    free_edge_table(edges);

    long int req_paths = opt.pq_size;
    npcl_path_pq h;
    npcl_path_pq_init(&h, req_paths + nnodes);

    size_t totlines = 0;


    double hmax = INFINITY;
    fprintf(stderr, "Calculating shortest paths from %ld nodes\n", nnodes);
    for (size_t node_no = 0; node_no < nnodes; node_no++) {

        size_t npaths = 0;
        double add_wt;

        if (is_simple) {
            add_wt = nodes2[node_no].value;
        } else if (is_invsimple) {
            add_wt = nodes2[node_no].value;
            add_wt = 1 / add_wt;
        } else {
            add_wt = 0;
        }

        if (node_no % 5000 == 0) {
            printf("NODE NO: %ld\n", node_no);
        }
       
        igraph_vector_ptr_t spath_res;

        init_edge_res(&spath_res, nnodes);
      
        igraph_get_shortest_paths_dijkstra(&G, NULL, &spath_res, node_no, igraph_vss_all(),
                                           &ewt_list, IGRAPH_OUT, NULL, NULL);

        edge_res_to_path_pq(&spath_res, &h, &edges2, opt.min_edges, req_paths, &npaths, add_wt, opt.decimals, &hmax);

        totlines += npaths;

        free_edge_res(&spath_res);

    }

    double final_lines = (opt.percentile * totlines) / 100;

    if (final_lines >= req_paths) {
        fprintf(stderr, "Rerun the analysis with a higher pq_size\n");
        exit(1);
    }

    char *spath_out_fname;
    asprintf(&spath_out_fname, "%s_%s", bname, "path_output.tsv");
    char *spath_out_file = npcl_path_join(output_path, spath_out_fname);

    char *edge_out_fname;
    asprintf(&edge_out_fname, "%s_%s", bname, "edge_output.tsv");
    char *edge_out_file = npcl_path_join(output_path, edge_out_fname);

    char *node_out_fname;
    asprintf(&node_out_fname, "%s_%s", bname, "node_output.tsv");
    char *node_out_file = npcl_path_join(output_path, node_out_fname);

    size_t totselect = 0;
    double final_value = 0;
    double first_loser = 0;

    int saturated = 1;
    pq_to_file(&h, spath_out_file, edge_out_file, node_out_file, &edges2, opt.pq_size, (long int) ceil(final_lines), opt.decimals, &totselect, &final_value, &first_loser, &saturated);
    free(spath_out_fname);
    free(spath_out_file);
    free(edge_out_fname);
    free(edge_out_file);
    free(node_out_fname);
    free(node_out_file);

    for (size_t i = 0; i < nedges; i++) {
        edge_entry e = edges2[i];
        free_edge_entry(e);
    }
    free(edges2);

    for (size_t i = 0; i < nnodes; i++) {
        node_entry n = nodes2[i];
        free(n.key);
    }
    free(nodes2);

    igraph_vector_destroy(&ewt_list);
    igraph_destroy(&G);

    npcl_path_pq_free(&h);

    char *log_fname;
    asprintf(&log_fname, "%s_%s", bname, "run_details.txt");
    char *log_path = npcl_path_join(output_path, log_fname);

    double end = omp_get_wtime();
    double ttaken = end - begin;
    fprintf(stderr, "Time elapsed for calculation: %.2lf\n", ttaken);
    write_spath_log(argc, argv, &opt, log_path, totlines, totselect, final_value, first_loser, ttaken, saturated, nnodes, nedges);
    free(log_fname);
    free(log_path);
    free(output_path);
    free(bname);

    opt_struct_free(&opt);
    fprintf(stderr, "Program has finished executing.\n");

    return 0;
}
