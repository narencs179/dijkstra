#define  _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

#include "../include/stb_ds.h"
#include "../include/npcl_strfn.h"
#include "../include/npcl_pq.h"
#include "../include/npcl_system.h"
#include "../include/igraph/igraph.h"

#include "helper_structs.h"
#include "helpers.h"
#include "ezmacros.h"

void read_nodefile(char *weighted_node_file, node_entry **node_table, char *sep) {

    FILE      *fcon    =   fopen(weighted_node_file, "r");
    size_t     bufsize =   0;
    char      *bufline =   NULL;
    size_t     curlen;
    size_t     nlines  =   0;

    size_t     nfaulty =   0;
    size_t     ndups   =   0;

    fprintf(stderr, "\nAttempting to parse the node file...\n");
    while ( (curlen = getline(&bufline, &bufsize, fcon)) != -1 ) {
        
        nlines++;

        if (nlines == 1) {
            continue;
        }

        // Skip empty lines
        if (curlen == 1) {
            continue;
        }

        npcl_trimws(bufline, curlen);

        npcl_str_arr array = npcl_strsplit(bufline, sep);

        if (array.filled < 2) {
            fprintf(stderr, "NODE ENTRY PARSING FAILURE - LINE %ld: ", nlines);
            if (array.filled == 1) {
                fprintf(stderr, "UNABLE TO GET NODE WEIGHT\n");
            } else {
                fprintf(stderr, "UNABLE TO GET NODE NAME AND WEIGHT\n");
            }
            nfaulty++;
            npcl_str_arr_free(&array);
            continue;
        }

        char *key = array.list[0];
        char *value_str = array.list[1];

        npcl_remove_quotes(key, strlen(key));
        npcl_remove_quotes(value_str, strlen(value_str));
        char *val_err;
        double value = strtod(value_str, &val_err);

        if (val_err[0] != '\0') {
            fprintf(stderr, "NODE ENTRY PARSING FAILURE - LINE %ld: ", nlines);
            fprintf(stderr, "THE INPUT IS NOT A VALID NUMERIC NODE WEIGHT\n");
            nfaulty++;
            npcl_str_arr_free(&array);
            continue;
        }
        node_entry s;

        long int key_i = shgeti(*node_table, key);
        if (key_i == -1) {
            s = create_node_entry(key, value, 1);
        } else {
            s = shgets(*node_table, key);
            s.n++;
            s.value  = value;
        }

        shputs(*node_table, s);

        npcl_str_arr_free(&array);

        if (key_i != -1) {
            fprintf(stderr, "DETECTED DUPLICATE NODE - LINE NO : %ld ", nlines);
            fprintf(stderr, "%s\n", s.key);
            ndups++;
            continue;
        }

    }

    free(bufline);
    fclose(fcon);

    if ( (ndups > 0) || (nfaulty > 0) ) {
        fprintf(stderr, "Number of faulty lines: %ld\n", nfaulty);
        fprintf(stderr, "Number of duplicate lines: %ld\n", ndups);
        fprintf(stderr, "Please fix these and rerun the program...\n");
        exit(1);
    } else {
        fprintf(stderr, "Node file was parsed successfully!\n\n");
    }

}

void free_node_table(node_entry *node_table) {
    for (size_t i = 0; i < shlen(node_table); i++) {
        free(node_table[i].key);
    }
    shfree(node_table);
}

void read_edgefile(char *weighted_edge_file, edge_entry **edge_table, node_entry **node_table, char *sep) {

    FILE *fcon = fopen(weighted_edge_file, "r");
    size_t bufsize = 0;
    char *bufline = NULL;

    size_t nlines = 0;
    size_t curlen;

    size_t ndups = 0;
    size_t nfaulty = 0;

    fprintf(stderr, "Attempting to parse weighted edge file\n");
    while ( (curlen = getline(&bufline, &bufsize, fcon)) != -1 ) {
        nlines++;

        if (nlines == 1) {
            continue;
        }

        // Skip empty lines
        if (curlen == 1) {
            continue;
        }

        npcl_trimws(bufline, curlen);

        npcl_str_arr array = npcl_strsplit(bufline, sep);

        if (array.filled < 3) {
            fprintf(stderr, "EDGE ENTRY PARSING ERROR - LINE NO %ld: ", nlines);
            if (array.filled == 2) {
                fprintf(stderr, "UNABLE TO PARSE EDGE WEIGHT\n");
            } else if (array.filled == 1) {
                fprintf(stderr, "UNABLE TO PARSE NODE B\n");
            } else {
                fprintf(stderr, "UNABLE TO PARSE NODES A AND B\n");
            }
            nfaulty++;
            npcl_str_arr_free(&array);
            continue;
        }

        char *nodeA_str = array.list[0];
        npcl_remove_quotes(nodeA_str, strlen(nodeA_str));

        char *nodeB_str = array.list[1];
        npcl_remove_quotes(nodeB_str, strlen(nodeB_str));

        char *ew_str    = array.list[2];
        char *ew_err;
        double ewt = strtod(ew_str, &ew_err);
        if (ew_err[0] != '\0') {
            nfaulty++;
            npcl_str_arr_free(&array);
            fprintf(stderr, "EDGE ENTRY PARSING ERROR - LINE NO %ld: ", nlines);
            fprintf(stderr, "Unable to parse edge weight.\n");
            continue;
        }

        char *key;
        asprintf(&key, "%s_%s", nodeA_str, nodeB_str);

        long int key_i = shgeti(*edge_table, key);

        if (shgeti((*node_table), nodeA_str) == -1) {
            node_entry t1 = create_node_entry(nodeA_str, 0, 1);
            shputs((*node_table), t1);
        }

        if (shgeti((*node_table), nodeB_str) == -1) {
            node_entry t2 = create_node_entry(nodeB_str, 0, 1);
            shputs((*node_table), t2);
        }


        edge_entry s;
        if (key_i == -1) {
            size_t nodeA_id = shgeti((*node_table), nodeA_str);
            size_t nodeB_id = shgeti((*node_table), nodeB_str);
            s = create_edge_entry(nodeA_str, nodeB_str, nodeA_id, nodeB_id, ewt, 1);

        } else {
            s = shgets(*edge_table, key);
            (s.n)++;
        }

        shputs(*edge_table, s);

        free(key);

        npcl_str_arr_free(&array);


        if (key_i != -1) {
            fprintf(stderr, "DETECTED DUPLICATE EDGE - LINE NO : %ld ", nlines);
            fprintf(stderr, "%s\t%s\n", s.nodeA, s.nodeB);
            ndups++;
        }
       
    }

    free(bufline);
    fclose(fcon);

    if ( (ndups > 0) || (nfaulty > 0) ) {
        fprintf(stderr, "Number of faulty lines: %ld\n", nfaulty);
        fprintf(stderr, "Number of duplicate edges: %ld\n", ndups);
        fprintf(stderr, "Please fix these and rerun the program...\n");
        exit(1);
    } else {
        fprintf(stderr, "Weighted edge file was parsed successfully!\n\n");
    }

}



/* Set up network based on a base network and a node file
 *
 * Given a node file and a base network file, subsets out a subnetwork where
 * each edge has both nodes present in the node file. Also subsets the node
 * table such that only nodes present in the subnetwork is present. The C igraph
 * library does not accept string names for node entries. The stb node table can
 * be traversed as if it were a normal array and the index of the node is used
 * as its unique numeric node ID. This is also reflected in the edge table,
 * making the input to igraph easy. Note that this function does not set up
 * edge weights. By default, each edge is initialized with a value of 0.
 *
 * @param basenet_file Base net file
 * @param edge_table    Variable containing edge table hash
 * @param weighted_node_file    Node weight file
 * @param node_table    Variable containing node table hash
 * @param sep           Separator for both the base net file and node file
 */
void setup_basenet(char *basenet_file, edge_entry **edge_table, char *weighted_node_file, node_entry **node_table, char *sep) {

    node_entry *init_nodes = NULL;

    read_nodefile(weighted_node_file, &init_nodes, sep);

    FILE *fcon = fopen(basenet_file, "r");
    size_t bufsize = 0;
    char *bufline = NULL;

    size_t nlines = 0;
    size_t curlen;

    size_t ndups = 0;
    size_t nfaulty = 0;

    fprintf(stderr, "Attempting to parse base network file\n");
    while ( (curlen = getline(&bufline, &bufsize, fcon)) != -1 ) {
        nlines++;

        if (nlines == 1) {
            continue;
        }

        // Skip empty lines
        if (curlen == 1) {
            continue;
        }

        npcl_trimws(bufline, curlen);

        npcl_str_arr array = npcl_strsplit(bufline, sep);

        if (array.filled < 2) {
            fprintf(stderr, "EDGE ENTRY PARSING ERROR - LINE NO %ld: ", nlines);
            if (array.filled == 1) {
                fprintf(stderr, "UNABLE TO PARSE NODE B\n");
            } else {
                fprintf(stderr, "UNABLE TO PARSE NODES A AND B\n");
            }
            nfaulty++;
            npcl_str_arr_free(&array);
            continue;
        }

        char *nodeA_str = array.list[0];
        char *nodeB_str = array.list[1];

        npcl_remove_quotes(nodeA_str, strlen(nodeA_str));
        node_entry nodeA = shgets(init_nodes, nodeA_str);
        if (nodeA.key == NULL) {
            npcl_str_arr_free(&array);
            continue;
        }

        npcl_remove_quotes(nodeB_str, strlen(nodeB_str));
        node_entry nodeB = shgets(init_nodes, nodeB_str);
        if (nodeB.key == NULL) {
            npcl_str_arr_free(&array);
            continue;
        }

        char *key;
        asprintf(&key, "%s_%s", nodeA_str, nodeB_str);

        long int key_i = shgeti(*edge_table, key);

        if (shgeti((*node_table), nodeA_str) == -1) {
            node_entry t1 = create_node_entry(nodeA.key, nodeA.value, 1);
            shputs((*node_table), t1);
        }

        if (shgeti((*node_table), nodeB_str) == -1) {
            node_entry t2 = create_node_entry(nodeB.key, nodeB.value, 1);
            shputs((*node_table), t2);
        }


        edge_entry s;
        if (key_i == -1) {
            size_t nodeA_id = shgeti((*node_table), nodeA_str);
            size_t nodeB_id = shgeti((*node_table), nodeB_str);
            s = create_edge_entry(nodeA_str, nodeB_str, nodeA_id, nodeB_id, 0, 1);

        } else {
            s = shgets(*edge_table, key);
            (s.n)++;
        }

        shputs(*edge_table, s);

        free(key);

        npcl_str_arr_free(&array);


        if (key_i != -1) {
            fprintf(stderr, "DETECTED DUPLICATE EDGE - LINE NO : %ld ", nlines);
            fprintf(stderr, "%s\t%s\n", s.nodeA, s.nodeB);
            ndups++;
            continue;
        }
       
    }

    free(bufline);
    fclose(fcon);

    if ( (ndups > 0) || (nfaulty > 0) ) {
        fprintf(stderr, "Number of faulty lines: %ld\n", nfaulty);
        fprintf(stderr, "Number of duplicate edges: %ld\n", ndups);
        fprintf(stderr, "Please fix these and rerun the program...\n");
        exit(1);
    } else {
        fprintf(stderr, "Base network file was parsed successfully!\n\n");
    }

//    for (size_t i = 0; i < shlen(init_nodes); i++) {
//        long int j = shgeti((*node_table), init_nodes[i].key);
//        if (j == -1) {
//            fprintf(stderr, "DISCARDING: %s\n", init_nodes[i].key);
//        }
//    }

    free_node_table(init_nodes);
    
}

/*
 * Free an edge table hash
 *
 * @param edge_table Hash table to be cleared
 */
void free_edge_table(edge_entry *edge_table) {
    for (size_t i = 0; i < shlen(edge_table); i++) {
        free_edge_entry(edge_table[i]);
    }
    shfree(edge_table);
}

// VERIFY GRAPH AND EDGE HASH TABLE MATCH
//    igraph_integer_t eid;
//    for (size_t i = 0; i < shlen(edges); i++) {
//        edge_entry e = edges[i];
//        igraph_get_eid(&G, &eid, e.nodeA_id, e.nodeB_id, IGRAPH_DIRECTED, 0);
//        printf("ENTRY IN EDGES: %ld\n", i);
//        printf("ENTRY IN GRAPH: %ld\n\n", (size_t) eid);
//    }

void setup_graph(edge_entry **edge_table, igraph_t *G, igraph_vector_t *wt_list) {

    igraph_vector_t edge_list;
    igraph_vector_init(&edge_list, shlen((*edge_table)) * 2);

    igraph_vector_init(wt_list, shlen((*edge_table)));

    for (size_t i = 0; i < shlen((*edge_table)); i++) {
        size_t i1 = 2 * i;
        size_t i2 = (2 * i) + 1;
        VECTOR(edge_list)[i1] = (*edge_table)[i].nodeA_id;
        VECTOR(edge_list)[i2] = (*edge_table)[i].nodeB_id;
        VECTOR((*wt_list))[i] = (*edge_table)[i].value;
    }

    igraph_create(G, &edge_list, 0, IGRAPH_DIRECTED);
    igraph_vector_destroy(&edge_list);

}

/*
 *
 * Enter the number of nodes in the graph for this. This should be a good
 * upper limit
 *
 * The initialization patterns are from examples/igraph_get_shortest_paths_dijkstra.c
 */
void init_edge_res(igraph_vector_ptr_t *edge_res, size_t nnodes) {

    igraph_vector_ptr_init(edge_res, nnodes);
    for (size_t i = 0; i < igraph_vector_ptr_size(edge_res); i++) {
        VECTOR((*edge_res))[i] = calloc(1, sizeof(igraph_vector_t));
        igraph_vector_init(VECTOR((*edge_res))[i], 0);
    }

}

void free_edge_res(igraph_vector_ptr_t *edge_res) {

    for (size_t i = 0; i < igraph_vector_ptr_size(edge_res); i++) {
        igraph_vector_destroy(VECTOR((*edge_res))[i]);
        free(VECTOR((*edge_res))[i]);
    }
    igraph_vector_ptr_destroy(edge_res);

}

void write_edge_res(igraph_vector_ptr_t *edge_res, FILE *fcon, edge_entry **edge_table, int min_edges, int ndec) {
    for (size_t i = 0; i < igraph_vector_ptr_size(edge_res); i++) {

        igraph_vector_t *v = VECTOR((*edge_res))[i];

        size_t n_edges = igraph_vector_size(v);
        if (n_edges < min_edges) {
            continue;
        }

        double total = 0;

        size_t init_e_id = VECTOR(*v)[0];
        fprintf(fcon, "\t%s", (*edge_table)[init_e_id].nodeA);
        fprintf(fcon, ",%s", (*edge_table)[init_e_id].nodeB);
        total += ((*edge_table)[init_e_id].value);

        for (size_t j = 1; j < n_edges; j++) {
            size_t e_id = VECTOR(*v)[j];
            fprintf(fcon, ",%s", (*edge_table)[e_id].nodeB);
            total += ((*edge_table)[e_id].value);
        }

        char *ne_str;
        asprintf(&ne_str, "\t%ld\t%lf\t%lf\n", n_edges, total, (total / ( (double) n_edges)));

        fprintf(fcon, "%s", ne_str);
        free(ne_str);

//        (*totpaths)++;

    }
}

double get_pq_max(npcl_pq *h) {
    if (h->nfilled == 0) {
        return (INFINITY);
    } else {
        double max = 0;
        for (long int i = 0; i < h->nfilled; i++) {
            if (max < h->list[i]->value) {
                max = h->list[i]->value;
            }
        }
        return max;
    }
}

void edge_res_to_path_pq(igraph_vector_ptr_t *edge_res, npcl_path_pq *h, edge_entry **edge_table, int min_edges, long int maxpqsize, size_t* npaths, double add_wt, int ndec, double *hmax) {

    for (size_t i = 0; i < igraph_vector_ptr_size(edge_res); i++) {

        igraph_vector_t *v = VECTOR((*edge_res))[i];

        size_t n_edges = igraph_vector_size(v);
        if (n_edges < min_edges) {
            continue;
        }

        double pcost = add_wt; // Is 0 if the metric used is not simple or inv_simple

        if (pcost != 0) {
            round_val(pcost, ndec);
        }

        size_t init_e_id = VECTOR(*v)[0];
        pcost += ((*edge_table)[init_e_id].value);

        for (size_t j = 1; j < n_edges; j++) {
            size_t e_id = VECTOR(*v)[j];
            pcost += ((*edge_table)[e_id].value);
        }
        round_val(pcost, ndec);

        double npcost = pcost / n_edges;
        round_val(npcost, ndec);
        
        if ((npcost < (*hmax))) { // Is this reliable? Is npcost < INFINITY robust?

            double elist[n_edges];

            for (long int i = 0; i < n_edges; i++) {
                elist[i] = VECTOR(*v)[i];
            }

            npcl_path path = npcl_create_path(elist, n_edges, pcost, npcost);
            if (h->nfilled >= h->capacity) {
//                npcl_path_pq_select(h, 0, h->nfilled - 1, maxpqsize - 1);
                qsort(h->list, h->nfilled, sizeof(npcl_path *), compare);
                (*hmax) = (h->list)[(maxpqsize - 1)]->value;
                npcl_path_pq_keep_n(h, maxpqsize);
            }
            npcl_path_pq_fill(h, &path);
            free(path.key);
        }
        (*npaths)++;
    }
}

void pq_to_file(npcl_path_pq *h, char *spath_out_file, char *edge_out_file, char *node_out_file, edge_entry **edge_table, long int pq_size, long int rpaths, int ndec, size_t *totselect, double *finval, double *firstlose, int *saturated) {

    qsort(h->list, h->nfilled, sizeof(npcl_path *), compare);
    double val = ((h->list)[(rpaths - 1)]->value);

    long int final_i = rpaths; // final_i needs to be set to the number of paths (ie, index + 1);
    for (long int i = rpaths; i < (h->nfilled) - 1; i++) {
        if ((((h->list)[i])->value) > val) {
            final_i = i;
            (*saturated) = 0;
            break;
        }
    }

// The queue that is input into this function (*h) should be a collection of
// queues that was handed out to each queue. As a result, it will be much
// larger than the individual queue size for each thread. The mathematics
// guarantees that if the final path that we select is found in less than the
// queue size for the individual thread, then we can guarantee that the path
// is a valid choice for whatever percentile of paths we're going for.
// Odds are, it still works when that isn't the case, but I'd rather that we're
// safe about the assumptions we make about the data
    if (final_i >= pq_size) {
        (*saturated) = 1;
    }

    if (*saturated) {
        fprintf(stderr, "The pq was insufficient to find all paths that matched the kth largest item\n");
//        exit(1);
    }


    (*finval) = val;
    if (*saturated) {
        (*firstlose) = ((h->list)[final_i - 1]->value);
    } else {
        (*firstlose) = ((h->list)[final_i]->value);
    }
    (*totselect) = final_i;

    npcl_path_pq_keep_n(h, final_i);

    node_entry *wnodes = NULL;
    edge_entry *wedges = NULL;

    size_t tot_nodes = 0;
    size_t tot_edges = 0;

    FILE *spath_out = fopen(spath_out_file, "w");
    FILE *edge_out  = fopen(edge_out_file , "w");
    FILE *node_out  = fopen(node_out_file , "w");
    fprintf(spath_out, "path_trace\tnedges\tpath_cost\tnorm_path_cost\tnew_nodes\tnew_edges\ttot_nodes\ttot_edges\n");
    fprintf(edge_out, "nodeA\tnodeB\tcurrent_path_cost\tlowest_path_cost\tpath_num\tdup_entry\n");
    fprintf(node_out, "node\tcurrent_path_cost\tlowest_path_cost\tpath_num\tdup_entry\n");
    for (size_t i = 0; i < (h->nfilled); i++) {

        size_t new_nodes  = 0;
        size_t new_edges = 0;

        double npcost = ((h->list)[i])->value;

        long int init_e_id = (((h->list)[i])->key)[0];
        fprintf(spath_out, "%s", (*edge_table)[init_e_id].nodeA);
        char *key = (*edge_table)[init_e_id].nodeA;
        double val = (*edge_table)[init_e_id].value;
        long int key_i1 = shgeti(wnodes, key);
        if (key_i1 == -1) {
            node_entry s = create_node_entry(key, npcost, 1);
            shputs(wnodes, s);
            new_nodes++;
            tot_nodes++;
            fprintf(node_out, "%s\t%.*lf\t%.*lf\t%ld\tFALSE\n", s.key, ndec, npcost, ndec, npcost, i + 1);
        } else {
            node_entry s = wnodes[key_i1];
            fprintf(node_out, "%s\t%.*lf\t%.*lf\t%ld\tTRUE\n", s.key, ndec, npcost, ndec, s.value, i + 1);

        }
        for (size_t j = 0; j < ((h->list)[i]->nedges); j++) {
            long int e_id = (((h->list)[i])->key)[j];
            char *key = (*edge_table)[e_id].nodeB;
            double val = (*edge_table)[e_id].value;
            long int key_i2 = shgeti(wnodes, key);
            if (key_i2 == -1) {
                node_entry t = create_node_entry(key, npcost, 1);
                shputs(wnodes, t);
                new_nodes++;
                tot_nodes++;
                fprintf(node_out, "%s\t%.*lf\t%.*lf\t%ld\tFALSE\n", t.key, ndec, npcost, ndec, npcost, i + 1);
            } else {
                node_entry t = wnodes[key_i2];
                fprintf(node_out, "%s\t%.*lf\t%.*lf\t%ld\tTRUE\n", t.key, ndec, npcost, ndec, t.value, i + 1);
            }

            char *key2 = (*edge_table)[e_id].key;
            long int key_i3 = shgeti(wedges, key2);
            if (key_i3 == -1) {
                edge_entry e  = (*edge_table)[e_id];
                edge_entry we = create_edge_entry(e.nodeA, e.nodeB, e.nodeA_id, e.nodeB_id, npcost, 1);
                shputs(wedges, we);
                new_edges++;
                tot_edges++;
                fprintf(edge_out, "%s\t%s\t%.*lf\t%.*lf\t%ld\tFALSE\n", we.nodeA, we.nodeB, ndec, npcost, ndec, npcost, i + 1);
            } else {
                edge_entry we = wedges[key_i3];
                fprintf(edge_out, "%s\t%s\t%.*lf\t%.*lf\t%ld\tTRUE\n", we.nodeA, we.nodeB, ndec, npcost, ndec, we.value, i + 1);
            }
            fprintf(spath_out, ",%s", (*edge_table)[e_id].nodeB);
        }
        fprintf(spath_out, "\t%ld\t%.*lf\t%.*lf\t%ld\t%ld\t%ld\t%ld\n", h->list[i]->nedges, ndec, h->list[i]->pcost, ndec, h->list[i]->value, new_nodes, new_edges, tot_nodes, tot_edges);
    }
    free_node_table(wnodes);
    free_edge_table(wedges);

    fclose(spath_out);
    fclose(edge_out);
    fclose(node_out);


}

int compare(const void *a, const void *b) {
    npcl_path *p1 = *((npcl_path **) a);
    npcl_path *p2 = *((npcl_path **) b);

    if ((p1->value) > (p2->value)) {
        return 1;
    } else if ((p1->value) < (p2->value)) {
        return -1;
    } else {
        if ((p1->nedges) < (p2->nedges)) {
            return 1;
        } else if ((p1->nedges) > (p2->nedges)) {
            return -1;
        } else { // All paths after this point have same number of edges
            for (long int i = 0; i < (p1->nedges); i++) {
                if ((((p1->key)[i]) - ((p2->key)[i])) > 0) {
                    return 1;
                } else if ((((p1->key)[i]) - ((p2->key)[i])) < 0) {
                    return -1;
                }
            }
            fprintf(stderr, "Possible presence of duplicate paths in output.\n");
            exit(1);
            return 0;
        }
    }
}

// I think that double should be accurate up to 15 decimal points. I'm using
// howmanyever is asked by the user + the remaining to make up 15 for the
// rounding. It gives a warning to the user if the chosen options are
// expected to give poor accuracy
double bankers_round(double val, long int decimals) {

    if (decimals > 14) {
        fprintf(stderr, "Rounding for > 14 decimal places will be inaccurate. Choose a different value.\n");
        exit(1);
    }

    long int precision = 15 - decimals;

    long int f = 1;
    for (size_t i = 0; i < decimals; i++) {
        f = f * 10;
    }

    long int p = 1;
    for (size_t i = 0; i < precision; i++) {
        p = p * 10;
    }

    long int v1 = val * f;
    long int v2 = ((long int) (val * f * p)) % p;
    int sign;
    if (v1 < 0) {
        sign = -1;
    } else {
        sign = +1;
    }
    v1 = v1 * sign;
    v2 = v2 * sign;

    int is_odd = (v1 % 2 != 0);

    long int cutoff = p / 2;


    if (v2 > cutoff) {
        v1++;
    } else if (v2 == cutoff) {
        if (is_odd) {
            v1++;
        }
    }

    double tmp = v1 / (double) f;
    tmp = tmp * sign;
    return tmp;
}
