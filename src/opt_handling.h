typedef struct opt_struct {
    long int pthreads;
    long int min_edges;
    int decimals;
    double percentile;
    long int pq_size;
//    double val_bound;
    double lower_bound;
    double upper_bound;
    char *base_net_file;
    char *node_weight_file;
    char *edge_weight_file;
    char *output_path;
    char *ewmetric;
    char *ewtransform;
    char *nwtransform;
} opt_struct;

void opt_struct_free(opt_struct *ostruct);
opt_struct options_parse(int argc, char **argv);
void opt_struct_print(opt_struct *ostruct);
void program_help(void);
