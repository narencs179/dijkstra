# Subgraph selection using Dijkstra Shortest Paths

Program that utilizes the C igraph library to implement Chandra lab's shortest paths based network methodology. Parallelized using OpenMP.