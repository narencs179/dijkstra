typedef struct npcl_str_arr {
    long int filled;
    long int capacity;
    char **list;
} npcl_str_arr;

npcl_str_arr npcl_str_arr_init(void);
void npcl_str_arr_add(npcl_str_arr *arr, char *s);
void npcl_str_arr_free(npcl_str_arr *arr);


void npcl_trimws(char *str, size_t slen);
void npcl_remove_char(char *str, size_t slen, char r);
void npcl_remove_quotes(char *str, size_t slen);
npcl_str_arr npcl_strsplit(char *line, char *sep);
void npcl_strcat(char **str1, char **str2);
