void write_nodekey_table(node_entry **node_table, char *fpath);
void write_node_weights(node_entry **node_table, char *fpath, long int ndec);
void write_edge_weights(edge_entry **edge_table, char *fpath, long int ndec);
void write_spath_log(int argc, char **argv, opt_struct *opt, char *fpath, size_t totpaths, size_t totselect, double finval, double firstlose, double ttaken, int saturated, long int nnodes, long int nedges);
