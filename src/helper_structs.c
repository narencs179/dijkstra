#define _GNU_SOURCE

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "helper_structs.h"

/* Constructs a node_entry object
 *
 * @param key   - Node name
 * @param value - Node weight
 * @param n     - Used to track the number of times node was seen in a file
 *
 */
node_entry create_node_entry(char *key, double value, size_t n) {
    node_entry tmp;

    if (key != NULL) {
        asprintf(&tmp.key, "%s", key);
    } else {
        tmp.key = NULL;
    }

    tmp.value = value;
    tmp.n     = n;

    return tmp;
}


void print_node_entry(node_entry node) {
    printf("NODE        : %s\n", node.key);
    printf("NODE WEIGHT : %lf\n", node.value);
    printf("NODE OCCUR  : %ld\n", node.n);

}

edge_entry create_edge_entry(char *nodeA, char *nodeB, size_t nodeA_id, size_t nodeB_id, double value, size_t n) {

    edge_entry tmp;

    bool nA_null = nodeA == NULL;
    bool nB_null = nodeB == NULL;
    bool both_present = (!nA_null) && (!nB_null);
    bool both_null    = ( nA_null) && ( nB_null);

    if (both_present) {
        asprintf(&tmp.nodeA, "%s", nodeA);
        asprintf(&tmp.nodeB, "%s", nodeB);
        asprintf(&tmp.key,   "%s_%s", nodeA, nodeB);
    } else if (both_null) {
        tmp.nodeA = NULL;
        tmp.nodeB = NULL;
        tmp.key   = NULL;
    } else {
        fprintf(stderr, "While creating an edge entry, either both nodes ");
        fprintf(stderr, "should be present or both nodes should be NULL.");
        fprintf(stderr, "Node A = %s | Node B = %s is not valid", nodeA, nodeB);
        exit(1);
    }
    tmp.n        = n;
    tmp.nodeA_id = nodeA_id;
    tmp.nodeB_id = nodeB_id;
    tmp.value    = value;
    return tmp;
}

/*
 * Free strings from an edge entry
 *
 * The edge key, nodeA string and nodeB string are present on the heap. This
 * function takes care of clearing them.
 *
 * @param edge edge_entry struct that needs to be freed
 */
void free_edge_entry(edge_entry edge) {
    free(edge.key);
    free(edge.nodeA);
    free(edge.nodeB);
}

void print_edge_entry(edge_entry edge) {
    printf("EDGE KEY    : %s\n", edge.key);
    printf("EDGE NODE A : %s\n", edge.nodeA);
    printf("EDGE NODE B : %s\n", edge.nodeB);
    printf("EDGE WEIGHT : %lf\n", edge.value);
}
